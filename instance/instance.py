from sanic import Sanic, response
from sanic.log import logger as logging

app = Sanic(name='Instance Server',)

@app.route('/video/<vid_id>/<playlist>')
async def vid(req, vid_id, playlist):
    ''' Get args after /
        localhost/video/666/playlist.m3u8
    '''
    logging.debug(f"INSTANCE Request /video/{vid_id}/{playlist}")
    return response.text(f'ID - {vid_id}, \n{playlist} were chosen')

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=80, debug=True)
