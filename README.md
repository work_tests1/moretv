# Python Load Balancer
Удалось создать асинхронный сервер на Python, исользуя только блиблиотки Sanic и Asyncio.

<details>
  <summary>ТЗ</summary>
  Нужно написать максимально _простой_ сервис-балансировщик пользовательских запросов, который должен отправлять пользователя с помощью 301-го HTTP редиректа смотреть фильм либо на корневые сервера, либо отправлять его в CDN по определённым правилам.  При этом сервис должен без проблем обрабатывать не менее 10000 запросов в секунду на среднем ноутбуке с core i5 7го поколения и 16GB RAM.
  
  ## Входящие данные
  Ожидается, что сервис будет обрабатывать входящие запросы вида: \
  `GET http://balancer-domain/?video=http://s1.origin-cluster/video/1488/xcg2djHckad.m3u8`
  
  Где:
  1. "balancer-domain" - хостнейм сервиса балансировки,
  2. http://s1.origin-cluster/video/1488/xcg2djHckad.m3u8 - URL видео-файла на сервере оригиналов s1, который нужно отдать пользователю либо через CDN, либо напрямую с севера оригиналов.
  
  ## Алгоритм обработки запросов
  1. Каждый 10й запрос, формата описанного выше, отправляем в оригинальный
  урл(query arg video) 301 редиректом.
  2. Остальные отправляем на `http://$CDN_HOST/s1/video/1488/xcg2djHckad.m3u8,`
  
  Где:
  - `s1` - сервер в кластере оригиналов
  - `$CDN_HOST` - настраиваемый через env url сервиса CDN
</details>

## Запуск
Сервер запускается командой `docker-compose up` создаются следующие контейнеры:
- Instance:
    > Рабочий сервер, который принимает запросы `GET /video/<id: int>/<playlist: str>` \
    > В докер композе создаюстся сразу 3 благожаря параметру `scale`
- CDN:
    > CDN Сервер, который принимает запросы `GET /<hostname: str>/video/<id: int>/<playlist: str>` \
    > По умолчанию стоит один сервер, но можно расширить
- Balancer:
    > Сервер балансировщик. Перенаправляет все входящие запросы по заданному url \
    > `GET /?video=http://<hostname: str>/video/<id: int>/<playlist: str>` \
    > Первые 10 запросов отправляет на `CDN` сервер, каждый 10й отправляется на сервер `Instance`

## Результаты тестированияя
С помощью утилиты [wrk](https://github.com/wg/wrk) прводил следующие тесты в течении 15ти сек

### 100 запросов в секунду
```
$./wrk -c100 -t1 -d15  http://localhost:5200/\?video\=http://moretv_instance_2/video/666/playlist.m3u8
Running 15s test @ http://localhost:5200/?video=http://moretv_instance_2/video/666/playlist.m3u8
  1 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.04s   231.82ms   1.34s    77.41%
    Req/Sec   117.36    101.44   607.00     92.86%
  1377 requests in 15.02s, 270.78KB read
Requests/sec:     91.69
Transfer/sec:     18.03KB
```

### 1000
```
$./wrk -c1000 -t1 -d15 http://localhost:5200/\?video\=http://moretv_instance_2/video/666/playlist.m3u8
Running 15s test @ http://localhost:5200/?video=http://moretv_instance_2/video/666/playlist.m3u8
  1 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.39s    15.59ms   1.42s    62.41%
    Req/Sec   799.00      1.26k    3.34k    85.71%
  1327 requests in 15.11s, 260.94KB read
  Socket errors: connect 0, read 0, write 0, timeout 1194
Requests/sec:     87.84
Transfer/sec:     17.27KB
```

### 10 000
```
$./wrk -c10000 -t1 -d15 http://localhost:5200/\?video\=http://moretv_instance_2/video/666/playlist.m3u8
Running 15s test @ http://localhost:5200/?video=http://moretv_instance_2/video/666/playlist.m3u8
  1 threads and 10000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     0.00us    0.00us   0.00us    -nan%
    Req/Sec     1.47k     2.05k    4.49k    80.00%
  1223 requests in 15.07s, 240.50KB read
  Socket errors: connect 8980, read 0, write 0, timeout 1223
Requests/sec:     81.16
Transfer/sec:     15.96KB
```
## P.S.
Что хочется добавить, было очень интересно. Сперва думал что мой сервис не выдержит и 100 запросов/с и на полпути где-то сломается.
Я не уверен, что я сделал все правильно и в полном соответствии с ТЗ, но думаю что именно это от меня требуется

