import random
import requests
import asyncio
from itertools import cycle
from urllib.parse import quote_plus

from sanic.response import HTTPResponse
from sanic import Sanic, response
from sanic.log import logger as logging
from sanic.exceptions import NotFound

from containers import get_instances_and_cdns


app = Sanic(name='BALANCER Server',)
counter = 0
to_server = 10

INSTANCES, CDNS = get_instances_and_cdns()
ITER = cycle(CDNS)
ALGORITHM = 'random'


@app.route('/')
async def balancer(req, ):
    logging.debug(f"BALANCER Request - {req.args}")
    #'video': ['http://s1/video/666/playlist']
    url = req.args.get('video')

    logging.info(f"REDIRECT TO - {url}")
    return await redirect_to_server(url)

async def redirect_to_server(url):
    ''' Redrect to instances
          [v] get url from balancer
          [v] parse url
          [v] find hostname in containers
             [v] If not found return 404
          [v] send info to container
    '''
    global counter
    global to_server
    global INSTANCES
    global CDNS
    global ALGORITHM
    logging.info(f"{counter} redirect")

    # ['http:', '', 's1', 'video', '666', 'playlist']
    hostname = url.split('/')[2]
    url_data = '/'.join(url.split('/')[3:])

    if not await check_hostname(hostname, INSTANCES):
        raise NotFound(f"Instance {hostname} Not Found")

    logging.info(f"Parsed - hostname={hostname}, url_data={url_data}")

    if counter == to_server: # 10
        counter = 0
        ip  = await get_instance_ip(hostname, INSTANCES)
        url_301 = f'http://{ip}/{url_data}'

    else:
        counter += 1
        ip  = await get_cdn_ip(CDNS, ALGORITHM)
        url_301 = f'http://{ip}/{hostname}/{url_data}'

    logging.debug(f"URL_301 - {url_301}")
    return await get_response(url_301)

async def check_hostname(hostname, dockers,):
    ''' Check if hostname - moretv_instance_48 in instances
    '''
    for host in dockers:
        if hostname == host['hostname']:
            return True
    logging.warning(f"hostname {hostname} NOT in Instances")
    return False

async def get_response(url):
    ''' Return response 301
        I decided to create own response to return exactly 301 answer
    '''
    safe_to = quote_plus(url, safe=":/%#?&=@[]!$&'()*+,;")
    headers = {"Location" : safe_to}
    resp = HTTPResponse(status=301,
                        headers=headers,
                        content_type='text/html; charset=utf-8'
                        )
    return resp

async def get_instance_ip(host, servers):
    ''' Get IP from servers list
        servers: list[dict, dict]
        [{'hostname': 'moretv_instance_1', 'ip':'172.27.0.4'}, ...]
    '''
    for server in servers:
        if host == server.get('hostname'):
            ip = server.get('ip')
            logging.debug(f"Found ip - {ip} for instance - {host}")
            return ip

async def get_cdn_ip(server_list, alg='random'):
    ''' I don't know how exactly chose CDN server
        Default choise is random
        But I can chose in infinite cycle
    '''
    global ITER
    if alg == 'random':
        cdn = random.choice(server_list)
    else:
        cdn = next(ITER)
    return cdn.get('ip')


if __name__ == '__main__':
   app.run(host='0.0.0.0', port=80, debug=True)
