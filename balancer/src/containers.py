import os
from docker import Client
import logging

def get_instances_and_cdns():
    ''' Collect info about all running instaces and cdn
    '''
    cli = Client(base_url='unix://var/run/docker.sock')
    HOSTNAME = os.environ.get("HOSTNAME") # '87fb04d9d55e'

    # Get info about my container
    my_container = [c for c in cli.containers() if c['Id'][:12] == HOSTNAME[:12]][0]
    # Get info about project name in my case - moretv as upper folder names
    project_name = my_container['Labels']['com.docker.compose.project']
    # Get info about network
    network_name = my_container['HostConfig']['NetworkMode']
    logging.debug(f"Collected info about running container")
    logging.debug(f"My Container - {my_container}")
    logging.debug(f"Project Name - {project_name}")
    logging.debug(f"Network Name - {network_name}")

    # get instances name and cdn names as hostname - moretv_instace_1/2/3...n, moretv_cdn_1
    instances = []
    cdns = []
    for cont in cli.containers():
        instance = {}
        if f'{project_name}_instance' in cont['Names'][0]:
            instance['hostname'] = cont['Names'][0].lstrip('/')
            instance['ip'] = cont['NetworkSettings']['Networks'][network_name]['IPAddress']
            instances.append(instance)
            logging.debug(f"Instance added - {instance}")

    for cont in cli.containers():
        cdn = {}
        if f'{project_name}_cdn' in cont['Names'][0]:
            cdn['hostname'] = cont['Names'][0].lstrip('/')
            cdn['ip'] = cont['NetworkSettings']['Networks'][network_name]['IPAddress']
            cdns.append(cdn)
            logging.debug(f"CDN Added - {cdn}")

    return instances, cdns
